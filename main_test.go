package main

import (
	"net/http"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	go func() {
		serve()
	}()
	r := m.Run()
	os.Exit(r)
}

func TestBar(t *testing.T) {
	client := http.Client{}
	req, _ := http.NewRequest("GET", "http://localhost:8081/bar", nil)
	resp, err := client.Do(req)
	if err != nil || resp.StatusCode != 200 {
		t.Fatal("TestBar error", err, resp)
	}
	resp.Body.Close()
}

func TestNotFount(t *testing.T) {
	client := http.Client{}
	req, _ := http.NewRequest("GET", "http://localhost:8081/", nil)
	resp, err := client.Do(req)
	if err != nil || resp.StatusCode != 404 {
		t.Fatal("TestNotFount error", err, resp)
	}
	resp.Body.Close()
}
