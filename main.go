package main

import (
	"log"
	"net/http"
)

func serve() {
	http.HandleFunc("/bar", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("hello world!\n"))
	})
	log.Fatal(http.ListenAndServe(":8081", nil))
}

func main() {
	serve()
}
