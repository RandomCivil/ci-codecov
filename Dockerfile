FROM golang:1.12.12-alpine

WORKDIR /go/src/ci-codecov

RUN apk add --no-cache --virtual .build-deps \
        git
RUN apk add --no-cache bash curl

ENV GO111MODULE=on CGO_ENABLED=0
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .

RUN rm -r ~/.cache/go-build/ \
    && apk del .build-deps
